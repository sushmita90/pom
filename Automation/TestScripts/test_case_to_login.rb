=begin
require 'selenium-webdriver'
require 'rubygems'
require '../PageObjects/Login'
require '../Base/TestBase'
require 'json'

class TestcaseForLogin

# get json string
  testDatafile = File.read($RootDirectToTestData+"TestData.json")

# parse and convert JSON to Ruby
  obj = JSON.parse(testDatafile)

#declaring the common variable globally to use across the file
  txtEmail = obj["email"]
  url = obj["url"]
  txtPassword = obj["password"]

#Instanciating the Login class object
  login = Login.new()

# Calling a test
  login.verifyLoginWithValidCredentials(txtEmail, txtPassword, url, $driver)

end=end
